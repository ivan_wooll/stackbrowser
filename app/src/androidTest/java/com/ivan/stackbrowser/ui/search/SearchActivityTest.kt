package com.ivan.stackbrowser.ui.search

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.clearText
import androidx.test.espresso.action.ViewActions.replaceText
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isEnabled
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.rule.ActivityTestRule
import com.ivan.stackbrowser.R
import org.hamcrest.CoreMatchers.not
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@LargeTest
class SearchActivityTest {

    @get:Rule
    val activityRule: ActivityTestRule<SearchActivity> =
        ActivityTestRule(SearchActivity::class.java)

    @Test
    fun whenSearchTextEmptySearchButtonDisabled() {
        onView(withId(R.id.editTextSearch))
            .perform(clearText())

        onView(withId(R.id.textViewSearchButton))
            .check(matches(not(isEnabled())))
    }

    @Test
    fun whenSearchTextPopulatedSearchButtonEnabled() {
        onView(withId(R.id.editTextSearch))
            .perform(replaceText("username"))

        onView(withId(R.id.textViewSearchButton))
            .check(matches(isEnabled()))
    }
}