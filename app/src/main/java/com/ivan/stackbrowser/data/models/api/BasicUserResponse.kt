package com.ivan.stackbrowser.data.models.api

data class BasicUserResponse(
    val items: List<StackExchangeUser>
)
