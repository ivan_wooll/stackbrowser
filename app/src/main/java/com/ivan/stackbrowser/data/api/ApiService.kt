package com.ivan.stackbrowser.data.api

import com.ivan.stackbrowser.data.models.api.BasicUserResponse
import com.ivan.stackbrowser.data.models.api.UserBadgesResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface ApiService {

    @GET("users")
    fun searchUsers(
        @Query("site") site: String = "stackoverflow",
        @Query("pagesize") pageSize: Int = 20,
        @Query("inname") userNameQuery: String
    ): Call<BasicUserResponse>

    @GET("users/{user_id}/badges")
    fun getUserBadges(
        @Path("user_id") userId: Long,
        @Query("site") site: String = "stackoverflow",
    ): Call<UserBadgesResponse>
}