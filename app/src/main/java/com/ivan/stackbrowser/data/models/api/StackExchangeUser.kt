package com.ivan.stackbrowser.data.models.api

import com.google.gson.annotations.SerializedName


data class StackExchangeUser(
    @SerializedName("user_id")
    val userId: Long,
    @SerializedName("display_name")
    val displayName: String,
    val reputation: Long,
    val location: String?,
    val age: Int?,
    @SerializedName("creation_date")
    val creationDate: Long,
    @SerializedName("profile_image")
    val profileImage: String
)
