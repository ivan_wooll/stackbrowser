package com.ivan.stackbrowser.data.models.api

data class UserBadgesResponse(
    val items: List<Badge>
)
