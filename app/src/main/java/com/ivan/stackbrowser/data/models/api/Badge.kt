package com.ivan.stackbrowser.data.models.api

data class Badge(val name: String)
