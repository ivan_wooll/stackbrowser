package com.ivan.stackbrowser.data.models.ui

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(
    val userId: Long,
    val userName: String,
    val reputation: String,
    val location: String,
    val age: String,
    val creationDate: String,
    val avatarUrl: String
) : Parcelable