package com.ivan.stackbrowser.ui.user_detail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import coil.load
import com.google.android.material.chip.Chip
import com.ivan.stackbrowser.data.models.ui.User
import com.ivan.stackbrowser.databinding.LayoutUserDetailBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class UserDetailActivity : AppCompatActivity() {

    companion object {
        fun getStartIntent(context: Context, user: User) =
            Intent(context, UserDetailActivity::class.java).apply {
                putExtras(bundleOf("user" to user))
            }
    }

    private val viewModel: UserDetailViewModel by viewModels()

    private val binding: LayoutUserDetailBinding by lazy {
        LayoutUserDetailBinding.inflate(layoutInflater)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewModel.userLiveData.observe(this) {
            it?.let { user ->
                println(user)
                binding.apply {
                    textViewUserName.text = user.userName
                    textViewReputation.text = user.reputation
                    textViewLocation.text = user.location
                    textViewAge.text = user.age
                    textViewMemberSince.text = user.creationDate
                    imageView.load(user.avatarUrl)
                }
            }
        }

        viewModel.userBadgesLiveData.observe(this) {
            it.forEach { badge ->
                val chip = Chip(this).apply { text = badge.name }
                binding.chipGroupBadges.addView(chip)
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}