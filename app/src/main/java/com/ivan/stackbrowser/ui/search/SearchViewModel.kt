package com.ivan.stackbrowser.ui.search

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ivan.stackbrowser.data.api.ApiService
import com.ivan.stackbrowser.data.models.api.StackExchangeUser
import com.ivan.stackbrowser.data.models.ui.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.text.DateFormat

class SearchViewModel @ViewModelInject constructor(
    private val apiService: ApiService
) : ViewModel() {

    val userListItems = MutableLiveData<List<User>>()
    val errorLiveData = MutableLiveData<String>()

    @Suppress("BlockingMethodInNonBlockingContext") // we are happy to suppress this warning as we know that this is a safe call
    fun searchForUser(queryString: String) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                // todo: shorten the number of lines of code by unwrapping the API response in to a generic object.
                //  This would be better done in a Repository class than here
                try {
                    val response = apiService.searchUsers(userNameQuery = queryString).execute()
                    if (response.isSuccessful) {
                        val list = response.body()?.items
                        if (!list.isNullOrEmpty()) {
                            val users = list.map { user ->
                                println(user)
                                mapUserToListItem(user)
                            }.sortedBy { userListItem -> userListItem.userName }
                            userListItems.postValue(users)
                        } else {
                            errorLiveData.postValue("the api call was rejected")
                        }
                    } else {
                        errorLiveData.postValue("no items found!")
                    }
                } catch (exception: Exception) {
                    errorLiveData.postValue("there was an error making the api call")
                    exception.printStackTrace()
                }
            }
        }
    }

    // todo: this doesn't really belong here as the formatting of the data for display should not be
    //  the responsibility of the ViewModel. Move this to a dedicated mapping class
    private fun mapUserToListItem(user: StackExchangeUser): User {
        val age = user.age?.toString() ?: "Prefer not to say"
        val creationDate = DateFormat.getDateTimeInstance()
            .format(user.creationDate * 1000) // creationDate is a Unix timestamp measured in seconds so convert to milliseconds

        return User(
            userId = user.userId,
            userName = user.displayName,
            reputation = "Reputation: ${user.reputation}",
            location = user.location ?: "Somewhere on the internet!",
            age = "Age: $age",
            creationDate = "Member since: $creationDate",
            avatarUrl = user.profileImage
        )

    }


}