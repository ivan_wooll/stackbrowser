package com.ivan.stackbrowser.ui.search

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.ivan.stackbrowser.data.models.ui.User
import com.ivan.stackbrowser.databinding.ListItemUserBinding

class UserListAdapter(val callbacks: Callbacks) :
    ListAdapter<User, UserListAdapter.StackUserViewHolder>(StackUserDiffCallback()) {

    interface Callbacks {
        fun onStackUserClick(user: User)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StackUserViewHolder {
        val binding =
            ListItemUserBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return StackUserViewHolder(binding)
    }

    override fun onBindViewHolder(holder: StackUserViewHolder, position: Int) {
        val stackUser = getItem(position)
        holder.bind(stackUser)
    }

    class StackUserDiffCallback : DiffUtil.ItemCallback<User>() {
        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem.userId == newItem.userId

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean =
            oldItem == newItem

    }

    inner class StackUserViewHolder(private val binding: ListItemUserBinding) :
        RecyclerView.ViewHolder(binding.root) {

        init {
            binding.root.setOnClickListener {
                val stackUser = getItem(adapterPosition)
                callbacks.onStackUserClick(stackUser)
            }
        }

        fun bind(stackUser: User) {
            binding.textViewUserNumber.text = stackUser.userId.toString()
            binding.textViewUserName.text = stackUser.userName
        }
    }
}
