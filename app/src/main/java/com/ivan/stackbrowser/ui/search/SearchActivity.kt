package com.ivan.stackbrowser.ui.search

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import com.ivan.stackbrowser.data.models.ui.User
import com.ivan.stackbrowser.databinding.LayoutMainBinding
import com.ivan.stackbrowser.extensions.afterTextChanged
import com.ivan.stackbrowser.extensions.hideKeyboard
import com.ivan.stackbrowser.ui.user_detail.UserDetailActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SearchActivity : AppCompatActivity(), UserListAdapter.Callbacks {

    private val viewModel: SearchViewModel by viewModels()

    private val binding: LayoutMainBinding by lazy { LayoutMainBinding.inflate(layoutInflater) }

    private val userListAdapter by lazy { UserListAdapter(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.apply {
            recyclerViewUsers.adapter = userListAdapter

            editTextSearch.afterTextChanged { text ->
                textViewSearchButton.isEnabled = text.isNotEmpty()
                if (text.isEmpty()) {
                    // clear the list when text is cleared
                    userListAdapter.submitList(emptyList())
                }
            }

            textViewSearchButton.setOnClickListener {
                val text = editTextSearch.text.toString()
                viewModel.searchForUser(text)
                textViewSearchButton.hideKeyboard()
            }

        }

        viewModel.userListItems.observe(this) { stackUsers: List<User> ->
            userListAdapter.submitList(stackUsers)
        }

        viewModel.errorLiveData.observe(this) {
            Toast.makeText(this, it, Toast.LENGTH_LONG).show()
            println(it)
        }


    }

    override fun onStackUserClick(user: User) {
        startActivity(UserDetailActivity.getStartIntent(this, user))
    }


}