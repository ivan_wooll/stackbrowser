package com.ivan.stackbrowser.ui.user_detail

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.ivan.stackbrowser.data.api.ApiService
import com.ivan.stackbrowser.data.models.api.Badge
import com.ivan.stackbrowser.data.models.ui.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class UserDetailViewModel @ViewModelInject constructor(
    @Assisted val savedStateHandle: SavedStateHandle,
    private val apiService: ApiService
) : ViewModel() {

    private val user: User? by lazy { savedStateHandle.get<User>("user") }

    val userLiveData = MutableLiveData<User>()
    val userBadgesLiveData = MutableLiveData<List<Badge>>()

    init {
        userLiveData.postValue(user)

        user?.let {
            fetchUserBadges(it.userId)
        }
    }

    @Suppress("BlockingMethodInNonBlockingContext") // we are happy to suppress this warning as we know that this is a safe call
    private fun fetchUserBadges(userId: Long) {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                // todo: move this to a repository class and fetch a full list of paged results.
                //  Currently this fetches the first page of any results at the default size
                try {
                    val response = apiService.getUserBadges(userId = userId).execute()
                    if (response.isSuccessful) {
                        response.body()?.items?.let {
                            userBadgesLiveData.postValue(it)
                        }
                    }
                } catch (exception: Exception) {
                    // todo: The failure to get badges is not important enough to notify the user.
                    //  log the exception to crash reporting service
                    exception.printStackTrace()
                }
            }
        }
    }

}