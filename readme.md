# Stack Browser


## by Ivan Wooll

Android test project for candyspace.com

## Requirements and Constraints

- [x] Use a git repository to track your progress
- [x] Minimum Android API 19
- [x] The project must compile by executing gradle with `lint test assembleDebug`
- [ ] Use RxJava(2) for handling async network calls `*`
- [x] Use Retrofit2 for http requests
- [x] Use Kotlin instead of Java
- [x] Bonus points if you write any form of documentation (wiki, request for comments, readme, etc.)
- [x] Bonus points if you write any UI tests `**`


`*` I haven't used `RXJava` for a number of years and so am not 100% confident in current best practices when using it in the network layer. Due to time constraints and the learning curve involved in producing a decent implementation using `RxJava` I opted to use `Kotlin coroutines`.

`**` A UI test for the `SearchActivity` can be found in the `src/androidTest` directory.